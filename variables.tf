variable vpc_id {
    description = "VPC id"
    type = string
}

variable name_prefix {
    description = "Name prefix of Security Group"
    type = string
}

variable ingress_port {
    description = "ingress port in Security Group"
    type = number
}

variable ingress_cidr_blocks {
    description = "ingress cidr block in Security Group"
    type = string
}